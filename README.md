# Find your local football team

A collection of maps that make it easy for people to find the closest football team from any given point. It uses Voronoi diagrams [rendered with D3](https://d3js.org/) overlayed on a [Leaflet map](https://leafletjs.com/). You can find the working version [here](https://guushoekman.com/local-football-team/).

This project has extensively used code from [Chris Zetter's Voronoi map](https://github.com/zetter/voronoi-maps).

## Explanation

A thorough explanation of the code has been provided already by Chris Zetter and can be found [here](http://chriszetter.com/blog/2014/06/15/building-a-voronoi-map-with-d3-and-leaflet/). In order to create your own Voronoi map with different data, the main component that needs to be changed is the .csv file; you will need the a name, the latitude and longitude, and assign a type (which will appear in the menu).

### Changes to Leaflet

In order to ensure the Voronoi diagrams are rendered correctly at all zoom levels, Leaflet's `latLngToLayerPoint` function has been changed slightly.

By default, Leaflet rounds points to the nearest integer. This is usually not a problem, but on higher zoom levels this distorts the location of teams and as a consequence messes up the corresponding areas. As such, rounding has been removed from the `latLngToLayerPoint` function.

[An issue was opened on GitHub in 2016](https://github.com/Leaflet/Leaflet/issues/4745) but there hasn't been any activity since 2018.

## Contribute

Contributions are very welcome! Adding a country or league is easy and quick. Please check out the [contribute page](https://gitlab.com/guushoekman/local-football-team/-/blob/master/CONTRIBUTING.md) for more information.

## License

This code is released under the terms of the MIT license.