---
layout: base
title: Contribute
---
<div class="contribute container">
  <h1>Find your local football team</h1>
  <a href="{{ site.baseurl }}/">Back to homepage</a>

  <h2>How to contribute</h2>
  <p>
    The easiest way to contribute to the site is to make a spreadsheet with a country's data or add a league to an existing country. Doing this is quite straightforward.
  </p>

  <h3>Get some spreadsheet software</h3>
  <p>
     What we need is a CSV file with the relevant data. The easiest way is using a spreadsheet application. If you don't have one I can recommend <a href="https://www.libreoffice.org/discover/calc/">LibreOffice</a> and, if you're using Linux, <a href="http://www.gnumeric.org/">Gnumeric</a>.
  </p>

  <p>
    Take a look at <a href="{{ site.baseurl }}/example.csv">this example file</a> which contains the data for England. The first column lists the club name. The second and third are the position of the stadium, its latitude and longitude respectively. The fourth column lists the competition the team plays in.
  </p>

  <h3>Collect the data</h3>
  <p>
    You can find the location of a club's stadium in many ways. I've found the easiest way to be going to the Wikipedia page of the competition, which virtually always lists the participating teams and their stadiums. From there you can go to the stadium's page, which almost always has the coordinates.
  </p>

  <p>
    <strong>I have already done many in the 2015/2016 season</strong>. You can find them in <a href="https://guushoekman.com/local-football-team/data/input.csv">this CSV file</a>. It's not entirely up-to-date, but most teams are still in the same league and play in the same stadium, so it's a good starting point.
  </p>

  <p>
    Please note I need the coordinates in decimal format. The coordinates on the Wiki page is a link to a page that lists them in the right format. For example, here is the page for Ajax' <a target="_blank" href="https://tools.wmflabs.org/geohack/geohack.php?pagename=Johan_Cruyff_Arena&params=52_18_51_N_4_56_31_E_region:NL_source:nlwiki">Johan Cruijff Arena</a> which lists its decimal coordinates as 52.314167, 4.941944.
  </p>

  <p>
    Of course you can also use <a target="_blank" href="https://www.openstreetmap.org/">OpenStreetMap</a>, <a href="https://maps.google.com/">Google Maps</a>, and many other map providers. I've just found copy-pasting things from those websites quite cumbersome, but whatever works for you.
  </p>

  <h3>Send it to me</h3>
  <p>
    When you've finished the file please send it to me at <a href="mailto:mail@guushoekman.com">mail@guushoekman.com</a>.
  </p>

  <p>
    That's really all there is to it. A few other things need to be done to create a new page for a country, but it's easier if I take care of that. It doesn't take long and I'll let you know when it's done.
  </p>

  <h2 id="mistake">I've noticed a mistake</h2>

  <p>
    Please help me fix it by sending an email with the details to <a href="mailto:mail@guushoekman.com">mail@guushoekman.com</a>. Thanks!
  </p>
</div>