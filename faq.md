---
layout: base
title: FAQ
---
<div class="faq container">
  <h1>Find your local football team</h1>
  <a href="{{ site.baseurl }}/">Back to homepage</a>

  <h2>Frequently asked questions</h2>

  <p class="question">
    What is this website?
  </p>
  <p class="answer">
    It's a collection of maps that help you find your local football team.
  </p>

  <p class="question">
    Can you help me interpret the maps?
  </p>
  <p class="answer">
    What you see on top of the maps are called <a href="https://en.wikipedia.org/wiki/Voronoi_diagram" target="_blank">Voronoi diagrams</a>. Every dot is the location of a team's stadium. The cell around each dot is the area in which no other dot is closer. The dividing lines are the exact halfway point between two stadiums.
  </p>

  <p class="question">
    Can you add [insert country]?
  </p>
  <p class="answer">
    Sure, but I'm pretty lazy and busy with other things. It would probably be much quicker if <i><b>you</b></i> added it! It's super simple. All you need to do is put some data in a spreadsheet. Head on over to the <a href="{{ site.baseurl }}/contribute.html">contribute page</a> to find out how.
  </p>

  <p class="question">
    Why did you make this?
  </p>
  <p class="answer">
    A general interest in football, data visualisations, and Voronoi diagrams.
  </p>

  <p class="question">
    I'd like to make something similar with different data. Can I?
  </p>
  <p class="answer">
    Absolutely. Everything is open source and <a target="_blank" href="https://gitlab.com/guushoekman/local-football-team">available on GitLab</a>. Just keep in mind the MIT license requires attribution, including to <a target="_blank" href="https://github.com/zetter/voronoi-maps">Chris Zetter</a> whose code I've used to create this.
  </p>

  <p class="question">
    I've got another question. Can I get in touch with you?
  </p>
  <p class="answer">
    Sure, but please don't spam me: <a href="mailto:mail@guushoekman.com">mail@guushoekman.com</a>.
  </p>
</div>