# How to contribute

The easiest way to contribute to the site is to make a spreadsheet with a country's data or add a league to an existing country. Doing this is quite straightforward.

## Get some spreadsheet software

All you need to do is create a CSV file with the relevant data. The easiest way is using a spreadsheet application. If you don't have one I can recommend [LibreOffice](https://www.libreoffice.org/discover/calc/) and, if you're using Linux, [Gnumeric](http://www.gnumeric.org/).

Take a look at [this example file](https://gitlab.com/guushoekman/local-football-team/-/blob/master/example.csv) which contains the data for England. The first column lists the club name. The second and third are the position of the stadium, its latitude and longitude respectively. The fourth column lists the competition the team plays in.

## Collect the data

You can find the location of a club's stadium in many ways. I've found the easiest way to be going to the Wikipedia page of the competition, which virtually always lists the participating teams and their stadiums. From there you can go to the stadium's page, which almost always has the coordinates.

**I have already done many in the 2015/2016 season**. You can find them in [this CSV file](https://guushoekman.com/local-football-team/data/input.csv). It's not entirely up-to-date, but most teams are still in the same league and play in the same stadium, so it's a good starting point.

Please note I need the coordinates in decimal format. The coordinates on the Wiki page is a link to a page that lists them in the right format. For example, here is the page for Ajax' [Johan Cruijff Arena](https://tools.wmflabs.org/geohack/geohack.php?pagename=Johan_Cruyff_Arena&params=52_18_51_N_4_56_31_E_region:NL_source:nlwiki) which lists its decimal coordinates as 52.314167, 4.941944.

Of course you can also use [OpenStreetMap](https://www.openstreetmap.org/), [Google Maps](https://maps.google.com/), and many other map providers. I've just found copy-pasting things from those websites quite cumbersome, but whatever works for you.

## Send it to me

When you've finished the file please send it to me at [mail@guushoekman.com](mailto:mail@guushoekman.com). You can also do a pull request if that's easier.

That's really all there is to it. A few other things need to be done to create a new page for a country, but it's easier if I take care of that. It doesn't take long and I'll let you know when it's done.