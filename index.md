---
layout: base
---
<div class="home container">
  <h1>Find your local football team</h1>

  <div class="country-list">
    {%- for country in site.countries -%}
      <a href="{{ site.baseurl }}{{ country.url }}">
        <img src="{{ site.baseurl }}/img/{{ country.country }}.svg">
        <span class="text">{{ country.country | capitalize }}</span>
      </a>
    {%- endfor -%}
    <a href="{% link faq.md %}">
      <img class="icon" src="{{ site.baseurl }}/img/question.svg">
      <span class="text">What is this?</span>
    </a><a href="{% link contribute.md %}">
      <img class="icon" src="{{ site.baseurl }}/img/add.svg">
      <span class="text">Contribute</span>
    </a>
  </div>
</div>